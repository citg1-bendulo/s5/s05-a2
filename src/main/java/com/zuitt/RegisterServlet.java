package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")

public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3834552629113201265L;
	public void init()throws ServletException{
		System.out.println("Register Servlet is initialized");
		
		
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		//Capture the user input from booking form
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String email = req.getParameter("email");
		String phone = req.getParameter("phone");
		String discovery_type = req.getParameter("discovery_type");
		
		String birthday = req.getParameter("birthday");
		String user_type = req.getParameter("user_type");
		String description = req.getParameter("description");
		
		
		//Store all the data from the booking form into the session;
		HttpSession session=req.getSession();
		session.setAttribute("firstName", firstName);
		session.setAttribute("lastName", lastName);
		session.setAttribute("email", email);
		session.setAttribute("phone", phone);
		session.setAttribute("discovery_type", discovery_type);
		session.setAttribute("birthday", birthday);
		session.setAttribute("user_type", user_type);
		session.setAttribute("description", description);
		
		
		
		res.sendRedirect("register.jsp");

	}
	
	public void destroy() {
		System.out.println("Register Servlet has been finalized");
	}
}
