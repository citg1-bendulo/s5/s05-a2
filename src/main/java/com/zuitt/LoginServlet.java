package com.zuitt;

import java.io.IOException;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8286177122798244808L;
	
public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		//Capture the user input from booking form
		HttpSession session= req.getSession();
		String fullName=session.getAttribute("firstName").toString()+" "+session.getAttribute("lastName").toString();
		session.setAttribute("fullName", fullName);
		
		
		res.sendRedirect("home.jsp");

	}

}
