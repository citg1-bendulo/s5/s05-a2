<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Welcome Page</title>
	</head>
	<body>
		<h1>Welcome <%=session.getAttribute("fullName") %></h1>
		<%
			String welcomeMessage="";
			String type= session.getAttribute("user_type").toString();
			if(type.equals("applicant")){
				welcomeMessage="Welcome Applicant. You may now start looking for career oppurtunities";
			}
			else{
				welcomeMessage="Welcome Employer. You may now start browsing applicant profiles";
			}
		%>
		<p><%=welcomeMessage %></p>
	</body>
</html>