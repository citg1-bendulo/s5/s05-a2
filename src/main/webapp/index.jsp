<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
		<head>
		<meta charset="ISO-8859-1">
		<title>Insert title here</title>
	</head>
	<body>
		
		<div>
		<h1>Welcome to Servlet Job Finder</h1>
		<form action="register" method="post">
			<!-- Name -->
				<div>
					<label for="firstName">First name:</label>
					<input type="text" name="firstName" required>
				</div>	
				<div>
					<label for="lastName">Last Name:</label>
					<input type="text" name="lastName" required>
				</div>
				<div>
					<label for="phone">Phone:</label>
					<input type="tel" name="phone" required>
				</div>
				<div>
					<label for="email">Email:</label>
					<input type="email" name="email" required>
				</div>
				
				
				<fieldset>
					<legend>How did you discover this app?</legend>
					<!--  Taxi-->
					<input type="radio" id="discovery" name="discovery_type" value="friends" required>
					<label for="friends">Friends</label>
					<br>
					<!-- four seater-->
					<input type="radio" id="discovery" name="discovery_type" value="social_media" required>
					<label for="social_media">Social Media</label>
					<br>
					<!-- Six Seater-->
					<input type="radio" id="discovery" name="discovery_type" value="others" required>
					<label for="others">Others</label>
					
					
				</fieldset>
				
				<div>
					<label for="birthday">Pickup Date/Time</label>
					<input type="date" name="birthday" required>	
					
					
				</div>
				<div>
					<label for="user_type">Are you an employer or applicant</label>
					<select id="employee_type" name="user_type">
						<!-- <option value="" selected="selected">Select One</option> -->
						<option value="applicant">Applicant</option>
						<option value="employer">Employer</option>
					</select>
				</div>
				<div>
					<label for="description">Profile Description</label>
					<textarea name="description" maxlength="500"></textarea>
					
				
				</div>
				
				<button>Register</button>
		
		</form>
		
		</div>
	
	</body>
</html>