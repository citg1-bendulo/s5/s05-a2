<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Confirmation</title>
		</head>
	<body>
		<%
			
			String type= session.getAttribute("discovery_type").toString();
			if(type.equals("social_media")){
				type="Social Media";
			}
			else if(type.equals("others")){
				type="Others";
			}
			else{
				type="Friends";
			}
		%>
		<h1>Register confirmation</h1>
		<p>First Name:<%=session.getAttribute("firstName") %></p>
		<p>Last Name:<%=session.getAttribute("lastName") %></p>
		<p>Phone:<%=session.getAttribute("phone") %></p>
		
		<p>Email:<%=session.getAttribute("email") %></p>
		<p>App Discovery:<%=type %></p>
		<p>Date of Birth:<%=session.getAttribute("birthday") %></p>
		<p>User Type:<%=session.getAttribute("user_type") %></p>
		<p>Description:<%=session.getAttribute("description") %></p>
		
		<form action="login" method="post">
			<input type="submit"></input>
		</form>
		<form action="index.jsp" >
			<input type="submit" value="Back"/>
		</form>
	</body>
</html>